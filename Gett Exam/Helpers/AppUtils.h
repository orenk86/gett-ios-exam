//
//  AppUtils.h
//  Gett Exam
//
//  Created by Oren Kosto on 11/6/17.
//  Copyright © 2017 Oren Kosto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@interface AppUtils : NSObject

+(AppUtils *) sharedUtils;

+(NSString *) getLocalConfiguration:(NSString *)name;

+(void) setLocalConfiguration:(NSString *)configuration forKey:(NSString *)key;

+(void) saveDefaultValue:(NSString *)value forKey:(NSString *)key;

+(NSString *) getDefaultValueFofKey:(NSString *)key;

+(void) performSelector:(SEL)selector on:(id)delegate;

+(void) performSelector:(SEL)selector on:(id)delegate withObject:(id)object;

+(BOOL)hasVisitedApp;

@end
