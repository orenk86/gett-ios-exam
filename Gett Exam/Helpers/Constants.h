//
//  Constants.h
//  Gett Exam
//
//  Created by Oren Kosto on 11/6/17.
//  Copyright © 2017 Oren Kosto. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#define GOOGLE_GEOCODE_API_KEY @"GoogleGeoCodeApiKey"
#define GOOGLE_MAPS_API_KEY @"GoogleMapsApiKey"
#define GOOGLE_PLACES_API_KEY @"GooglePlacesApiKey"

#define GOOGLE_GEOCODE_REQUEST_URL @"https://maps.googleapis.com/maps/api/geocode/json"
#define GOOGLE_PLACES_REQUEST_URL @"https://maps.googleapis.com/maps/api/place/nearbysearch/json"

#define DEFAULTS_KEY_HAS_VISITED_APP @"hasVisitedApp"
#define CONFIG_KEY_GOOGLE_PLACES_RADIUS @"googlePlacesRadius"
#define CONFIG_KEY_MONITOR_REACHABILITY @"monitorReachability"

#define NOTIFICATION_LOCATION_PERMISSION_STATE_CHANGE @"locationPermissionStateChangedNotification"
#define NOTIFICATION_LOCATION_CHANGE @"locationChangedNotification"
#define NOTIFICATION_LOCATION_NAME_CHANGE @"locationNameChangedNotification"
#define NOTIFICATION_PLACES_CHANGE @"placesChangedNotification"

#define TEXT_PERMISSIONS_ENABLED @"Location permissions are enabled. Have fun! :)"
#define TEXT_PERMISSIONS_NOT_ENABLED @"Please allow access to your location for proper functionality of the app."
#define TEXT_PERMISSION_RESTRICTED_TITLE @"Uh Oh!"
#define TEXT_PERMISSION_RESTRICTED_MESSAGE @"It looks like we aren't able to enable location permissions from here. Please enable them manually from the settings app."
#define TEXT_OPEN_SETTINGS @"Open Settings"
#define TEXT_MAYBE_LATER @"Maybe Later"
#define TEXT_ALERT_OK @"OK"
#define TEXT_ALERT_CANCEL @"Cancel"
#define TEXT_ALERT_ERROR_TITLE @"Error!"
#define TEXT_ALERT_ERROR_MESSAGE @"Oops! Something went wrong. Please try again."

#endif /* Constants_h */
