//
//  AppDelegate.m
//  Gett Exam
//
//  Created by Oren Kosto on 11/4/17.
//  Copyright © 2017 Oren Kosto. All rights reserved.
//

#import "AppDelegate.h"
#import "AppUtils.h"
#import <GoogleMaps/GoogleMaps.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    //These API keys can also be set via a settings.plist file in the main bundle
    [AppUtils setLocalConfiguration:@"AIzaSyBIKvQXSWk9_U1JuzW6W1RfMa42O3j57fs" forKey:GOOGLE_GEOCODE_API_KEY];
    [AppUtils setLocalConfiguration:@"AIzaSyB7CNAWuWVBiFfcnhFZhfa3f3SXlJcMj0c" forKey:GOOGLE_MAPS_API_KEY];
    [AppUtils setLocalConfiguration:@"AIzaSyCRE2KHvlc1g887sGOjOwXSzOuRAmd62ek" forKey:GOOGLE_PLACES_API_KEY];
    
    [GMSServices provideAPIKey:[AppUtils getLocalConfiguration:GOOGLE_MAPS_API_KEY]];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
