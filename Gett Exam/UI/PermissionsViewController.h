//
//  PermissionsViewController.h
//  Gett Exam
//
//  Created by Oren Kosto on 11/6/17.
//  Copyright © 2017 Oren Kosto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PermissionsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *instructionsLabel;
@property (weak, nonatomic) IBOutlet UIButton *requestAccessButton;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;

@end
