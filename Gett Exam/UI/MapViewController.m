//
//  MapViewController.m
//  Gett Exam
//
//  Created by Oren Kosto on 11/6/17.
//  Copyright © 2017 Oren Kosto. All rights reserved.
//

#import "MapViewController.h"
#import "LocationHelper.h"
#import "Constants.h"
#import "NSDictionary+Gett.h"
#import "UIView+Gett.h"

@interface MapViewController ()

@property (strong, nonatomic) GMSCameraPosition *camera;
@property (strong, nonatomic) GMSMarker *currentLocationMarker;
@property (strong, nonatomic) NSMutableDictionary <NSString *, GMSMarker *> *placesMarkers;

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.mapView.myLocationEnabled = YES;
    self.mapView.delegate = self;
    self.placesMarkers = [NSMutableDictionary dictionaryWithCapacity:20];
}

-(void)viewWillAppear:(BOOL)animated {
    [self updateLocation];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationHasChanged:) name:NOTIFICATION_LOCATION_CHANGE object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationNameHasChanged:) name:NOTIFICATION_LOCATION_NAME_CHANGE object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(placesHaveChanged:) name:NOTIFICATION_PLACES_CHANGE object:nil];
}

-(void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//refresh the user's current location
-(void)updateLocation {
    if ([LocationHelper sharedInstance].currentLocation != nil) {
        if (self.camera == nil) {
            self.camera = [GMSCameraPosition cameraWithTarget:[LocationHelper sharedInstance].currentLocation.coordinate
                                                         zoom:15];
            self.mapView.camera = self.camera;
        }
        
        if (self.currentLocationMarker == nil) {
            self.currentLocationMarker = [[GMSMarker alloc] init];
        }
        
        self.currentLocationMarker.position = [LocationHelper sharedInstance].currentLocation.coordinate;
        self.currentLocationMarker.title = @"Me";
        self.currentLocationMarker.map = self.mapView;
    }
}

-(void)updatePlaces {
    if ([LocationHelper sharedInstance].places != nil && [LocationHelper sharedInstance].places.count > 0) {
//        [self.mapView clear];
        [self updateLocation];
//        [self.placesMarkers removeAllObjects];
        for (NSDictionary *place in [LocationHelper sharedInstance].places) {
            NSNumber *lat = [place safeValueForKeyPath:@"geometry.location.lat"];
            NSNumber *lng = [place safeValueForKeyPath:@"geometry.location.lng"];
            NSString *placeId = place[@"id"];
            //only add newly discovered places, while keeping the existing ones on the map
            if (lat != nil && lng != nil && placeId != nil && self.placesMarkers[placeId] == nil) {
                GMSMarker *marker = [[GMSMarker alloc] init];
                marker.position = CLLocationCoordinate2DMake(lat.doubleValue, lng.doubleValue);
                marker.title = place[@"name"] ? place[@"name"] : @"";
                marker.snippet = place[@"vicinity"] ? place[@"vicinity"] : @"";
                marker.appearAnimation = kGMSMarkerAnimationPop;
                marker.map = self.mapView;
                self.placesMarkers[placeId] = marker;
            }
        }
    }
}

- (void)locationHasChanged:(NSNotification *)notification {
    [self updateLocation];
}

- (void)locationNameHasChanged:(NSNotification *)notification {
    self.locationNameLabel.text = [LocationHelper sharedInstance].currentLocationName;
}

- (void)placesHaveChanged:(NSNotification *)notification {
    [self updatePlaces];
}

#pragma mark mapview delegate
-(void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture {
    [self.centerMapIcon fadeIn];
}

#pragma mark center the map
- (IBAction)centerMap:(UIButton *)sender {
    self.mapView.camera = self.camera;
    [self.centerMapIcon fadeOut];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
