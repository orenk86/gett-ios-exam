//
//  ViewController.m
//  Gett Exam
//
//  Created by Oren Kosto on 11/4/17.
//  Copyright © 2017 Oren Kosto. All rights reserved.
//

#import "LoadingViewController.h"
#import "ViewControllerSwitch.h"
#import "LocationHelper.h"
#import "AppUtils.h"

@interface LoadingViewController ()

@end

@implementation LoadingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self enterApplication];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)enterApplication {
    //skip the welcome screen if this is not the first time the user has opened the app.
    //skip the permissions screen if location permission has been granted.
    NSString *nextVcName;
    if ([AppUtils hasVisitedApp]) {
        nextVcName = [[LocationHelper sharedInstance] hasPermission] ? @"HomeViewController" : @"PermissionsViewController";
    } else {
        nextVcName = @"WelcomeViewController";
    }
    
    UIViewController *nextVc = [self.storyboard instantiateViewControllerWithIdentifier:nextVcName];
    
    [ViewControllerSwitch loadController:nextVc withOptions:UIViewAnimationOptionTransitionCrossDissolve];
}

@end
