//
//  ListViewController.m
//  Gett Exam
//
//  Created by Oren Kosto on 11/9/17.
//  Copyright © 2017 Oren Kosto. All rights reserved.
//

#import "ListViewController.h"
#import "PlaceTableViewCell.h"
#import "LocationHelper.h"
#import "NSDictionary+Gett.h"
#import "Constants.h"

#define TABLEVIEW_CELL_REUSE_IDENTIFIER @"PlaceTableViewCell"

@interface ListViewController ()

@end

@implementation ListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorColor = [UIColor clearColor];
}

-(void)viewWillAppear:(BOOL)animated {
    [self.tableView reloadData];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(placesHaveChanged:) name:NOTIFICATION_PLACES_CHANGE object:nil];
}

-(void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)placesHaveChanged:(NSNotification *)notification {
    [self.tableView reloadData];
}

#pragma mark tableview datasource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PlaceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:TABLEVIEW_CELL_REUSE_IDENTIFIER forIndexPath:indexPath];
    if ([LocationHelper sharedInstance].places != nil && [LocationHelper sharedInstance].places.count > indexPath.row) {
        [cell setPlace:[LocationHelper sharedInstance].places[indexPath.row]];
    }
    return cell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [LocationHelper sharedInstance].places == nil ? 0 : [LocationHelper sharedInstance].places.count;
}

#pragma mark tableview delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
