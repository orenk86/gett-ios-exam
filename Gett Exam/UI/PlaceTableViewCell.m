//
//  PlaceTableViewCell.m
//  Gett Exam
//
//  Created by Oren Kosto on 11/9/17.
//  Copyright © 2017 Oren Kosto. All rights reserved.
//

#import "PlaceTableViewCell.h"
#import "NSDictionary+Gett.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface PlaceTableViewCell()

@property(nonatomic, strong) NSDictionary *place;

@end

@implementation PlaceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setPlace:(NSDictionary *)place {
    _place = place;
    self.nameLabel.text = place[@"name"] ? place[@"name"] : @"";
    self.vicinityLabel.text = place[@"vicinity"] ? place[@"vicinity"] : @"";
    if (place[@"icon"]) {
        [self.iconImage sd_setImageWithURL:place[@"icon"]];
    } else {
        [self.iconImage setImage:nil];
    }
}

@end
