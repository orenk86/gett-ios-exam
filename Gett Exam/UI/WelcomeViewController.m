//
//  WelcomeViewController.m
//  Gett Exam
//
//  Created by Oren Kosto on 11/6/17.
//  Copyright © 2017 Oren Kosto. All rights reserved.
//

#import "WelcomeViewController.h"
#import "AppUtils.h"
#import "ViewControllerSwitch.h"
#import "LocationHelper.h"
#import "UIView+Gett.h"

#define LABEL_SWITCH_INTERVAL 4

@interface WelcomeViewController ()

@property (nonatomic, strong) NSArray <UILabel *> *introLabels;
@property (nonatomic, strong) NSTimer *introLabelTimer;
@property (nonatomic, assign) int currentIntroLabel;

@end

@implementation WelcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.introLabels = @[self.introLabel1, self.introLabel2, self.introLabel3, self.introLabel4];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self startTimer];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self stopTimer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)startTimer {
    self.introLabelTimer = [NSTimer scheduledTimerWithTimeInterval:LABEL_SWITCH_INTERVAL target:self selector:@selector(switchIntroLabel:) userInfo:nil repeats:YES];
}

-(void)stopTimer {
    if (self.introLabelTimer && [self.introLabelTimer isValid]) {
        [self.introLabelTimer invalidate];
    }
}

-(void)switchIntroLabel:(NSTimer *)timer {
    //show the next intro label
    self.currentIntroLabel = (self.currentIntroLabel + 1) % self.introLabels.count;
    for (int i = 0; i < self.introLabels.count; i++) {
        if (i == self.currentIntroLabel) {
            [self.introLabels[i] fadeIn];
        } else {
            [self.introLabels[i] fadeOut];
        }
    }
}

- (IBAction)getStarted:(id)sender {
    [AppUtils saveDefaultValue:@"YES" forKey:DEFAULTS_KEY_HAS_VISITED_APP];
    
    //skip the permissions screen if location permission has been granted.
    NSString *nextVcName = [[LocationHelper sharedInstance] hasPermission] ? @"HomeViewController" : @"PermissionsViewController";
    UIViewController *nextVc = [self.storyboard instantiateViewControllerWithIdentifier:nextVcName];
    
    [ViewControllerSwitch loadController:nextVc withOptions:UIViewAnimationOptionTransitionCrossDissolve];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
