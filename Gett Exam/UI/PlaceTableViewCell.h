//
//  PlaceTableViewCell.h
//  Gett Exam
//
//  Created by Oren Kosto on 11/9/17.
//  Copyright © 2017 Oren Kosto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlaceTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *vicinityLabel;

- (void)setPlace:(NSDictionary *)place;

@end
