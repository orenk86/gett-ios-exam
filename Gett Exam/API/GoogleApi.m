//
//  GoogleApi.m
//  Gett Exam
//
//  Created by Oren Kosto on 11/6/17.
//  Copyright © 2017 Oren Kosto. All rights reserved.
//

#import "GoogleApi.h"
#import "Constants.h"
#import <AFNetworking/AFNetworking.h>
#import "AppUtils.h"

//https://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452&key=AIzaSyBIKvQXSWk9_U1JuzW6W1RfMa42O3j57fs

@interface GoogleApi()

@property (strong, nonatomic) NSURLSessionConfiguration *configuration;
@property (strong, nonatomic) AFURLSessionManager *manager;

@end

@implementation GoogleApi

static GoogleApi *sharedInstance = nil;
static dispatch_once_t onceToken;
static BOOL isSetup = NO;

+(GoogleApi *) sharedInstance
{
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    if (!isSetup) {
        [sharedInstance setup];
        isSetup = YES;
    }
    return sharedInstance;
}

-(void)setup {
    self.configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    self.manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:self.configuration];
}

-(void)requestReverseGeocodeForLocation:(CLLocation *)location success:(void (^)(NSDictionary * _Nonnull))success failure:(void (^)(NSError * _Nullable))failure {
    NSString *latLng = [NSString stringWithFormat:@"%f,%f", location.coordinate.latitude, location.coordinate.longitude];
    NSDictionary *params = @{
                             @"latlng": latLng,
                             @"key": [AppUtils getLocalConfiguration:GOOGLE_GEOCODE_API_KEY]
                             };
    
    NSURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET"
                                                                          URLString:GOOGLE_GEOCODE_REQUEST_URL
                                                                         parameters:params
                                                                              error:nil];
    NSURLSessionDataTask *task = [self.manager dataTaskWithRequest:request
                                                 completionHandler:^(NSURLResponse * _Nonnull response, NSDictionary * _Nullable responseObject, NSError * _Nullable error) {
                                                     if (error
                                                         || !responseObject[@"results"]
                                                         || ![responseObject[@"results"] isKindOfClass:[NSArray class]]) {
//                                                         NSLog(@"Geocode error);
                                                         failure(error);
                                                     } else {
//                                                         NSLog(@"Geocode response: %@", response);
//                                                         NSLog(@"Geocode response object: %@", responseObject);
                                                         NSArray *results = responseObject[@"results"];
                                                         if (results.count > 0) {
                                                             success(results[0]);
                                                         } else {
                                                             failure(error);
                                                         }
                                                     }
                                                 }];
    
    NSLog(@"Requesting reverse geocoding for location: %@", latLng);
    [task resume];
}

-(void)requestPlacesForLocation:(CLLocation *)location withType:(NSString * _Nullable)type success:(void (^)(NSArray <NSDictionary *> * _Nonnull))success failure:(void (^)(NSError * _Nullable))failure
{
    NSString *latLng = [NSString stringWithFormat:@"%f,%f", location.coordinate.latitude, location.coordinate.longitude];
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{
                                                                                  @"location": latLng,
                                                                                  @"radius": [AppUtils getLocalConfiguration:CONFIG_KEY_GOOGLE_PLACES_RADIUS],
                                                                                  @"key": [AppUtils getLocalConfiguration:GOOGLE_PLACES_API_KEY]
                                                                                  }];
    if (type) {
        params[@"type"] = type;
    }
    
    NSURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET"
                                                                          URLString:GOOGLE_PLACES_REQUEST_URL
                                                                         parameters:params
                                                                              error:nil];
    NSURLSessionDataTask *task = [self.manager dataTaskWithRequest:request
                                                 completionHandler:^(NSURLResponse * _Nonnull response, NSDictionary * _Nullable responseObject, NSError * _Nullable error) {
                                                     if (error
                                                         || !responseObject[@"results"]
                                                         || ![responseObject[@"results"] isKindOfClass:[NSArray class]]) {
                                                         failure(error);
                                                     } else {
                                                         NSArray *results = responseObject[@"results"];
                                                         success(results);
                                                     }
                                                 }];
    
    NSLog(@"Requesting places for location: %@", latLng);
    [task resume];
}

@end
