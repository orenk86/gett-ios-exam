//
//  GoogleApi.h
//  Gett Exam
//
//  Created by Oren Kosto on 11/6/17.
//  Copyright © 2017 Oren Kosto. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface GoogleApi : NSObject

+(GoogleApi * _Nonnull) sharedInstance;

-(void)requestReverseGeocodeForLocation:(CLLocation * _Nonnull)location
                                success:(nullable void(^)(NSDictionary * _Nonnull result))success
                                failure:(nullable void(^)(NSError * _Nullable e))failure;

-(void)requestPlacesForLocation:(CLLocation * _Nonnull)location
                       withType:(NSString * _Nullable)type
                        success:(nullable void (^)(NSArray <NSDictionary *> * _Nonnull))success
                        failure:(nullable void (^)(NSError * _Nullable))failure;

@end
